import java.io.*;

public class ReadFromStream {
  public void readFromStream(InputStream is) throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(is));
    String line;
    while((line = br.readLine()) != null) {
      System.out.println(line);
    }
  }
}
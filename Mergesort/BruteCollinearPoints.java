import edu.princeton.cs.algs4.StdDraw; 
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.In;

public class BruteCollinearPoints {
   
    private int numberOfSegments;
    private LineSegment[] segments;
    
    // finds all line segments containing 4 points
    public BruteCollinearPoints(Point[] points)  {
        if (points == null) throw new IllegalArgumentException("points can not be null"); 
        
        numberOfSegments = 0;
        segments = new LineSegment[points.length];
        
        Point[] pointsCopy = new Point[points.length];
        for (int i = 0; i < points.length; i++) {
            if(points[i] == null) throw new IllegalArgumentException("point can not be null");
            pointsCopy[i] = points[i];
        }
        
        sortPoints(pointsCopy);
        
        for (int i = 0; i < pointsCopy.length; i++) {
            Point p = pointsCopy[i];
            
            for (int j = i+1; j < pointsCopy.length; j++) {
                Point q = pointsCopy[j];
                double slopePQ = p.slopeTo(q);
                
                for (int k = j+1; k < pointsCopy.length; k++) {
                    Point r = pointsCopy[k];
                    double slopePR = p.slopeTo(r);        
                    if (Double.compare(slopePQ, slopePR) != 0) {
                        continue;
                    }
                    for (int m = k+1; m < pointsCopy.length; m++) {
                        Point s = pointsCopy[m];
                        double slopePS = p.slopeTo(s);
                        if (Double.compare(slopePQ, slopePS) == 0) {
                            LineSegment segment = new LineSegment(p, s);
                            segments[numberOfSegments++] = segment;
                        }
                    }
                }
            }
        }
    }
   
     // the number of line segments
    public int numberOfSegments() {
        return numberOfSegments;
    }
    
     // the line segments
    public LineSegment[] segments()  {
        LineSegment[] segmentsCopy = new LineSegment[numberOfSegments];
        for(int i = 0; i < numberOfSegments; i++) segmentsCopy[i] = segments[i];
        return segmentsCopy;
    }
    
    // insertion sort Routine to sort points
    private void sortPoints(Point[] points) {
        // insertion sort on points
        for (int i = 0; i < points.length; i++) {
            for (int j = i; j > 0; j--) {
                Point a = points[j];
                Point b = points[j-1];
                
                if (a == null) throw new IllegalArgumentException("point can not be null");
                if (b == null) throw new IllegalArgumentException("points can not be null"); 
                if (a.compareTo(b) == 0) throw new IllegalArgumentException("points can not be equal");
                
                if (a.compareTo(b) < 0) exch(points, j, j-1);
            }
        }
    }
    
    private void exch(Point[] a, int i, int j) {
        Point temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }
    
    
    public static void main(String[] args) {
        // read the n points from a file
        In in = new In("collineartest/input8.txt");
        int n = in.readInt();
        Point[] points = new Point[n];
        for (int i = 0; i < n; i++) {
            int x = in.readInt();
            int y = in.readInt();
            points[i] = new Point(x, y);
        }

        // draw the points
        StdDraw.enableDoubleBuffering();
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        for (Point p : points) {
            p.draw();
        }
        StdDraw.show();
        
        // print and draw the line segments
        BruteCollinearPoints collinear = new BruteCollinearPoints(points);
        for (LineSegment segment : collinear.segments()) {
            StdOut.println(segment);
            segment.draw();
        }
        StdDraw.show();
    }
    
}

import java.util.Iterator;
import java.util.NoSuchElementException;
import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdOut;

public class RandomizedQueue<Item> implements Iterable<Item> {
    
    private Item[] q;       // queue elements
    private int n;          // number of elements on queue
    
    // construct an empty randomized queue
    public RandomizedQueue() {
        q = (Item[]) new Object[2];
        n = 0;
    }
    
    // is the queue empty?
    public boolean isEmpty() {
        return n == 0;
    }
    
    // return the number of items on the queue
    public int size() {
        return n;
    }
    
     // add the item
    public void enqueue(Item item) {
        if (item == null) throw new IllegalArgumentException("Item must not be null"); 
        // double size of array if necessary and recopy to front of array
        if (n == q.length) resize(2*q.length);   // double size of array if necessary
        q[n++] = item;                        // add item
    }
    
    // remove and return a random item
    public Item dequeue() {
        if (isEmpty()) throw new NoSuchElementException("queue is empty");
        
        int i = StdRandom.uniform(n);
        Item item = q[i];
        q[i] = q[--n]; 
        q[n] = null; // to avoid loitering
        // shrink size of array if necessary
        if (n > 0 && n == q.length/4) resize(q.length/2); 
        return item;
    }
    
    // return (but do not remove) a random item
    public Item sample() {
        if (isEmpty()) throw new NoSuchElementException("queue is empty");  
        return q[StdRandom.uniform(n)];
    }
    
    // return an independent iterator over items in random order
    public Iterator<Item> iterator() {
        return new ArrayIterator();
    }
    
    // an iterator, doesn't implement remove() since it's optional
    private class ArrayIterator implements Iterator<Item> {
        private int i = 0;
        private Item[] tempq;
        public boolean hasNext()  { 
           if (i == 0) {
               tempq = (Item[]) new Object[n];
               for(int j = 0; j < n; j++) tempq[j] = q[j];
               StdRandom.shuffle(tempq);
           }
           return i < n;                           
        }
        
        public void remove()      { throw new UnsupportedOperationException();  }

        public Item next() {
            if (!hasNext()) throw new NoSuchElementException();
            Item item = tempq[i++];
            return item;
        }
    }
    
    // resize the underlying array holding the elements
    private void resize(int capacity) {
        assert capacity >= n;
        Item[] temp = (Item[]) new Object[capacity];
        for (int i = 0; i < n; i++) {
            temp[i] = q[i];
        }
        q = temp;
    }

    // unit testing (optional)
    public static void main(String[] args) {
        RandomizedQueue<Integer> rq = new RandomizedQueue<Integer>();
         rq.enqueue(0);
      //  StdOut.println(rq.dequeue());
         rq.enqueue(7);
         rq.enqueue(14);
         rq.enqueue(38);
         rq.enqueue(23);
         rq.enqueue(43);
      //  StdOut.println(rq.size());
         rq.enqueue(26);
         rq.enqueue(35);
         rq.enqueue(9);
       // StdOut.println(rq.dequeue());
        
        for(Integer i:rq) {
            StdOut.println(i);
        }
        
//         StdOut.println(rq.dequeue());
//         StdOut.println(rq.sample());
//         StdOut.println(rq.size());
//         StdOut.println(rq.sample());
         //StdOut.println(rq.size());
         //StdOut.println(rq.sample());
         //   StdOut.println(rq.dequeue());//rq.dequeue();//     ==> 9
        
    }
}
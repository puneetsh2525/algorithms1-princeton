import java.util.Iterator;
import java.util.NoSuchElementException;
import edu.princeton.cs.algs4.StdOut;

public class Deque<Item> implements Iterable<Item> {
   
    private int n;          // size of the stack
    private Node first;     // top of stack
    private Node last;      // bottom of stack
    
    // helper linked list class
    private class Node {
        private Item item;
        private Node next;
        private Node prev;
    }
    
    // construct an empty deque
    public Deque()   {
        n = 0;
        first = null;
        last = null;
    }
    
    // is the deque empty?
    public boolean isEmpty() {
        return n == 0;
    }
    
     // return the number of items on the deque
    public int size() {
        return n;
    }
    
    // add the item to the front
    public void addFirst(Item item) {
        if (item == null) throw new IllegalArgumentException("Item must not be null"); 
        
        Node oldFirst = first; // get first 
        first = new Node(); // create new node and set as first
        first.item = item; // set item
        first.next = oldFirst; // set next for new node
        first.prev = null;
        
        if (oldFirst != null) oldFirst.prev = first;
        
        if (last == null) last = first;
        
        n++;
    }
    
     // add the item to the end
    public void addLast(Item item) {
        if (item == null) throw new IllegalArgumentException("Item must not be null"); 
        
        Node oldLast = last; // get last
        last = new Node(); // create new node and set as last
        last.item = item; // set item 
        last.next = null; // set next as nil
        last.prev = oldLast;
        
        // if old last exists, set its next to new last
        if (oldLast != null) oldLast.next = last; 
        
        if (first == null) first = last;
        
        n++;
    }
    
    // remove and return the item from the front
    public Item removeFirst() {
        if (isEmpty()) throw new NoSuchElementException("deck is empty");
        
        Item item = first.item;        // save item to return
        first = first.next;            // delete first node
        if (first != null) first.prev = null;
        
        if (first == null) last = first;
        
        n--;
        return item;                   // return the saved item
    }
    
    // remove and return the item from the end
    public Item removeLast() {
        if (isEmpty()) throw new NoSuchElementException("deck is empty");
        
        Item item = last.item;        // save item to return
        last = last.prev;            // delete last node
       
        if (last != null) last.next = null;
        
        if (last == null) first = last;
        n--;
        return item; // return the saved item
    }
    
    // return an iterator over items in order from front to end
    public Iterator<Item> iterator()  {
        return new ListIterator();
    }
    
    // an iterator, doesn't implement remove() since it's optional
    private class ListIterator implements Iterator<Item> {
        private Node current = first;
        public boolean hasNext()  { return current != null; }
        public void remove()      { throw new UnsupportedOperationException(); }

        public Item next() {
            if (!hasNext()) throw new NoSuchElementException();
            Item item = current.item;
            current = current.next; 
            return item;
        }
    }
    
    // unit testing (optional)
    public static void main(String[] args) {
        Deque<Integer> deque = new Deque<Integer>();
        deque.addFirst(0);
         StdOut.println(deque.removeLast());     
         deque.addFirst(2);
         deque.addFirst(3);
         deque.addFirst(4);
         deque.addFirst(5);
         StdOut.println(deque.removeLast());    
    }
}

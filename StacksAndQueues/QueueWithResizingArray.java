public class QueueWithResizingArray {
    
    private String[] queue;
    private int first;
    private int last;
    private int size;
    
    public QueueWithResizingArray() {
        queue = new String[1];
        first = 0;
        last = 0;
        size = 0;
    }
    
    public boolean isEmpty() {
        return first == last;
    }
    
    public String dequeue() {
        if(isEmpty()){return null;}
        ensureCapacityMin();
        
        String item = queue[first];
        queue[first] = null;
        
        first = (first+1) % queue.length;
        size--;
        
        return item;
     }
    
    public void enqueue(String item) {
        if(item == null) {
            throw new NullPointerException("Item must not be null");
        }
        
        ensureCapacityMax();
        
        queue[last] = item;
        last = (last+1) % queue.length;
        size++;
    }
    
    private void ensureCapacityMax() {
        if (size == queue.length) {
            resizeArray(2 * queue.length);
        }
    }
    
    private void ensureCapacityMin() {
        if (size < queue.length / 4) {
            resizeArray(queue.length / 2);
        }
    }
    
    private void resizeArray(int capacity){
        String[] copy = new String[capacity];
        for(int i = 0; i < queue.length; i++) {
            copy[i] = queue[i];
        }
        queue = copy;
    }
    
}
import edu.princeton.cs.algs4.WeightedQuickUnionUF;
import edu.princeton.cs.algs4.StdOut;


public class Percolation {
    
    private final int size;
    private final boolean[][] grid;
    private int openSitesCount;
    private final int top;
    private final int bottom;
    private final WeightedQuickUnionUF uf;
    
    // Backwash happens with top and bottom virtual site solution because all the open bottom sites
    // are connected with the bottom virtual site but not necessarily with the top. But there may be
    // some other bottom site which is connected with the top virtual site making the system percolate.
    // Now for all the open botom sites isFull methhod is going to return true.
    private final WeightedQuickUnionUF backwashUF; // creating another UF Ds to prevent backwash
    
    // create n-by-n grid, with all sites blocked 
    public Percolation(int n) {
        size = n;
        if (n <= 0) {
            throw new java.lang.IllegalArgumentException("n should be greater than zero");
        }
        grid = new boolean[n][n];
        openSitesCount = 0;
        top = 0;
        bottom = n*n+1;
        uf = new WeightedQuickUnionUF(n*n+2);
        backwashUF = new WeightedQuickUnionUF(n*n+1); // no bottom site for this UF
    }
    
    // open site (row, col) if it is not open already 
    public void open(int row, int col)  {
        checkRange(row, col);
        
        if (isOpen(row, col)) {
            return;
        }
        
        // Open
        grid[row-1][col-1] = true; // open site
        openSitesCount += 1;
        
        // if neighbouring sites are open, create a union 
        int index = xyTo1D(row, col);
        if (row == 1) { // create union with top
            uf.union(index, top);
            backwashUF.union(index, top);
        }
        
        if (row == size) { // create union with bottom
            uf.union(index, bottom); // no botton in backWashUF
        }
        
        // Check for left site, if open create union
        if (col != 1 && isOpen(row, col-1)) {
            uf.union(index, index-1);
            backwashUF.union(index, index-1);
        }
           
        // Check for right site, if open create union
        if (col != size && isOpen(row, col+1)) {
            uf.union(index, index+1);
            backwashUF.union(index, index+1);
        }
           
        // Check for top site, if open create union
        if (row != 1 && isOpen(row-1, col)) {
            int i = xyTo1D(row-1, col);
            uf.union(index, i);
            backwashUF.union(index, i);
        }
           
        // Check for bottom site, if open create union
        if (row != size && isOpen(row+1, col)) {
            int i = xyTo1D(row+1, col);
            uf.union(index, i);
            backwashUF.union(index, i);
        }
    }
    
    // is site (row, col) open?
    public boolean isOpen(int row, int col) {
        checkRange(row, col);
      
        return grid[row-1][col-1];
    }
    
    // is site (row, col) full?
    public boolean isFull(int row, int col)  {
        checkRange(row, col);
        
        int index = xyTo1D(row, col);
        return backwashUF.connected(index, top);
    }
  
       
     // number of open sites
    public int numberOfOpenSites()  {
        return openSitesCount;
    }
    
    // does the system percolate?
    public boolean percolates()   {
        return uf.connected(top, bottom);
    }

    // test client (optional)
    public static void main(String[] args) {
        Percolation p = new Percolation(2);
        
        p.open(1, 1);
        p.open(2, 1);
        
        StdOut.println(p.percolates());
    }
       
    // checks if row and col is in range
    private void checkRange(int row, int col) {
        int n = size;
        if (row < 1 || row > n) {
            throw new java.lang.IllegalArgumentException("row is out of bounds");
        }
        
        if (col < 1 || col > n) {
            throw new java.lang.IllegalArgumentException("col is out of bounds");
        }
    }
   
    // gives index for QuickUnion DS for given row and col
    private int xyTo1D(int row, int col) {
        int n = size;
        return n*(row-1)+col;
    }
}

import edu.princeton.cs.algs4.StdStats;
import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;

public class PercolationStats {
    private final int trials;
    private final double[] fractions;
    
    // perform trials independent experiments on an n-by-n grid
    public PercolationStats(int n, int trials) {
        if (n <= 0 || trials <= 0) {
            throw new IllegalArgumentException("n and trial should be greater than zero");
        }
        
        this.trials = trials;
        fractions = new double[trials];
        for (int i = 0; i < trials; i++) {
            Percolation p = new Percolation(n);
            
            while (!p.percolates()) {
                int row = StdRandom.uniform(1, n+1);
                int col = StdRandom.uniform(1, n+1);
                p.open(row, col);
            }
            
            double fraction = (double) p.numberOfOpenSites()/(n*n);
            fractions[i] = fraction;
        }
    }        

    // sample mean of percolation threshold
    public double mean() {
        return StdStats.mean(fractions);
    }
    
     // sample standard deviation of percolation threshold
    public double stddev() {
        return StdStats.stddev(fractions);
    }
    
    // low  endpoint of 95% confidence interval
    public double confidenceLo() {
        return mean() - ((1.96*stddev()) / Math.sqrt(trials));
    }
    
    // high endpoint of 95% confidence interval
    public double confidenceHi() {
        return mean() + ((1.96*stddev()) / Math.sqrt(trials));
    }

    // test client 
    public static void main(String[] args)  {
        int n = StdIn.readInt();
        int t = StdIn.readInt();
        
        try {
            
            PercolationStats stats = new PercolationStats(n, t);
        
            StdOut.println("mean                    = " + stats.mean());
            StdOut.println("stddev                  = " + stats.stddev());
            StdOut.println("95% confidence interval = [" + stats.confidenceLo() + ", " + stats.confidenceHi() + "]"); 
        } 
        catch (IllegalArgumentException e) {
            StdOut.println(e);
        }
        
    }
}
